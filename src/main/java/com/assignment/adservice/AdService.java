package com.assignment.adservice;

import db.model.tables.records.AdPageRecord;
import org.jooq.DSLContext;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static db.model.Tables.AD_PAGE;

@Service
public class AdService {

    DSLContext dslContext;

    public AdService(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    public List<AdPageResource> search(String searchTerm) {
        return dslContext.selectFrom(AD_PAGE)
                .where(AD_PAGE.KEYWORD.like("%"+searchTerm+"%"))
                .fetch()
                .stream()
                .map(this::convertEntityToResource)
                .collect(Collectors.toList());
    }

    private AdPageResource convertEntityToResource(AdPageRecord adPageRecord) {
        AdPageResource adPageResource = new AdPageResource();
        adPageResource.setAdvert(adPageRecord.getAdvert());
        adPageResource.setId(adPageRecord.getId());
        adPageResource.setKeyword(adPageRecord.getKeyword());

        return adPageResource;
    }
}
