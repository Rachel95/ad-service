package com.assignment.adservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/ad")
public class AdController {

    private final AdService service;

    public AdController(final AdService service) {
        this.service = service;
    }

    @GetMapping
    public List<AdPageResource> search(@RequestParam("query") String query) {
        return service.search(query);
    }
}
